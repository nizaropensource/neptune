# README #

This repo main purpose is continue working on neptune with other contributers.

Its a sample web application using neptune framework and PHP in backend.

Its not well formated, so its better to be hidden in a private repo, once we reach a good state, we will move to the main repo of neptune on github.
### We have exceeded the limit of the free account (5 users per team), so its public now to be visible to other people. ###

## Installation ##
- Run /ERD/restaurants.mwb on mysql-workbench to create the database.
- Copy /webapp/ to your web server dir.

Any changes to /webapp/controllers/neptuneJS2.js should be done on a separate branch, any other changes its ok to be on master.

# Issue Tracker is enabled, so no need to write in TODOs section, create issues instead #


# TODOs #

- neptune.prepareForm, change select all elements to querySelectorAll **[Pending Review]**
- Apply >=, <=, ... on filter in render_html
- In render html, add alias in filter 'new' **[Pending Review]**
- Spread_event needs optimization (look in angular)
- Consistency with camelCase **[Pending Review]**
- Create method model.applyOperation('operationName', optional callback) -- "nt-data-operation"
- Continue nt-data-bridges
- Apply sub model like, nt-data-model="restaurant.branches"
- extractModelsFrom() in case there is no nt-data-model attribute on element's children add else to call extractModelsFrom(obj, false).
- extractModelsFrom() in case filter = "new" remove %model.key% from the view ---> hold[i].innerHTML = renderHtmlWithModel(original_html, model_name, filter, alias).
- Add and update local model add_object_to_model().