<h3>
    Restaurants
</h3>

<div nt-data-model="restaurants">
    %id% - %name% - %city%
    <br/>
</div>

<div nt-data-model="restaurants" nt-data-filter="new" id="new_restaurant">
    <input class="form-control" name="name"/>
    <br/>
    <input class="form-control" name="city"/>
    <br/>
    <button class="btn" onclick="javascript:neptune.saveModel('new_restaurant')">Add</button>
</div>