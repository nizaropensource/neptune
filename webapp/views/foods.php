<h3>
    Foods
</h3>

<div nt-data-model="food_ingredients" nt-data-filter="new" id='add_food_ingredient'>
    <select nt-data-model="foods" name="food_id" id="food_id">
        <option value="%id%">%name%</option>
    </select>
    <select nt-data-model="ingredients" name="ingredient_id" nt-property-name="name">
        <option value="%id%">%name%</option>
    </select>
    <input class="form-control" type="text" name="quantity"/>
    <button class="btn" onclick="javascript:PF.foods.add_food_ingredients()">Add</button>
</div>
<br/>

<div nt-data-model="foods">
    %id% - %name%
    <br/>
    <ul nt-data-model="food_ingredients&food_id=%id%" nt-alias="food_ingrd">
        <li>%food_ingrd.name% - %quantity%</li>
    </ul>
</div>

<div nt-data-model="foods" id="new_food" nt-data-filter="new">
    <input class="form-control" type="text" name="name"/>
</div>

<button class="btn" onclick="neptune.saveModel('new_food')">Add</button>
<script type="text/javascript">
    PF.foods = {
        add_food_ingredients: function () {
            var food_id = document.getElementById('food_id').value;
            document.getElementById('add_food_ingredient').setAttribute('nt-data-model', 'food_ingredients&food_id=' + food_id);
            neptune.saveModel('add_food_ingredient');
        }
    };
</script>