<h3>
    Ingredients
</h3>

<div nt-data-model="ingredients">
    %id% - %name%
    <br/>
</div>

<div nt-data-model="ingredients" nt-data-filter="new" id="new_ingredient">
    <input class="form-control" name="name"/>
    <br/>
</div>

<button class="btn" onclick="javascript:neptune.saveModel('new_ingredient')">Add</button>