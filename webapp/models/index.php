<?php

session_start();
include_once('db.php');
include_once('model_interface.php');
include_once('model.php');

include_once('models/food_ingredients.model.php');
include_once('models/restaurants.model.php');
include_once('models/foods.model.php');
include_once('models/ingredients.model.php');
include_once('models/menu.model.php');

$model = $_GET['model'] . 'Model';
$operation = $_GET['operation'];

echo (new $model())->$operation();

?>