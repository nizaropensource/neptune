<?php

class Model {

    function generateRandomString($length = 10) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    function package($res, $model_name = 1) {
        $rows = array();
        if (is_a($res, "mysqli_result"))
            while ($row = $res->fetch_object()) $rows[] = $row;
        else $rows[] = $res;

        if ($model_name == 1)
            $model_name = $_GET["model"];

        foreach ($_GET as $key => $value) if ($key != "model" && $key != 'operation') $model_name .= "&$key=$value";

        $json_ret = '{"model":"' . $model_name . '", "data":' . json_encode($rows) . '}';

        return $json_ret;
    }

    function row_added($dict) {
        if (!isset($dict['data']))
            $dict['data'] = '';

        return json_encode($dict);
    }
    
	function row_modified($dict, $pk_name) {
		$dict['pk'] = $pk_name;
		if( !isset($dict['data']) )
			$dict['data'] = '';
		return json_encode($dict);
	}
	
    function package_dataTable2($q) {
        $rows = array();
        $total = 0;
        $filter = 0;

        $hold = explode("%d%", str_replace("%count%", "", $q));
        //print_r($hold);
        if (strpos($q, "group by") !== false) {
            $q = str_replace("group by", "#", $q);
            $q = str_replace("%d%", "", $q);
            $q = str_replace("%count%", ",(" . $hold[0] . "count(*) " . $hold[2] . ") as cnt", $q);
            $q .= $hold[3];
        }
        else {
            $q = str_replace("%d%", "", $q);
            $q = str_replace("%count%", ",(" . $hold[0] . "count(*) " . $hold[2] . ") as cnt", $q);
        }

        $q .= " limit $_GET[iDisplayStart] , $_GET[iDisplayLength]";
        //echo $q ;
        $res = execute_query($q);

        while ($row = $res->fetch_object()) {
            $arr = array();
            foreach ($row as $key => $value) {
                $arr[] = "$value";
            }
            $rows[] = $arr;
            $total = $row->cnt;
            $filter = $row->cnt;
        }

        //print_r($rows);
        $json_ret = '{"sEcho":"' . $_GET["sEcho"] . '", "iTotalRecords":"' . $total . '", "iTotalDisplayRecords":"' . $filter . '", "aaData":' . json_encode($rows) . '}';

        return $json_ret;
    }

    function package_dataTable($table, $search, $q = 1) {
        $rows = array();
        $total = 0;
        $filter = 0;
        if ($q == 1)
            $q = "select $table.*, (
                    select count(*) from $table where $search
                ) as cnt from $table where $search limit $_GET[iDisplayStart] , $_GET[iDisplayLength]";

        $res = execute_query($q);
        $cnt = execute_query_retuen_affected_rows($q);

        while ($row = $res->fetch_object()) {
            $arr = array();
            foreach ($row as $key => $value)
                $arr[] = "$value";
            $rows[] = $arr;
        }

        $total = $cnt;
        $filter = $cnt;

        $json_ret = '{"sEcho":"' . $_GET["sEcho"] . '", "iTotalRecords":"' . $total . '", "iTotalDisplayRecords":"' . $filter . '", "aaData":' . json_encode($rows) . '}';

        return $json_ret;
    }
}


?>