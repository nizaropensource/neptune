<?php

class food_ingredientsModel extends Model implements ModelInterface {
    public function Select() {
        return $this->package(execute_query("select * from food_ingredients
                                                inner join ingredients on food_ingredients.ingredients_id=ingredients.id
                                                where foods_id=$_GET[food_id]"));
    }

    public function Update() {
        food_ingredients::$foods_id = $_POST["food_id"];
        food_ingredients::$ingredients_id = $_POST["ingredient_id"];
        food_ingredients::$quantity = $_POST["quantity"];
        $ret["id"] = execute_query_return_id(food_ingredients::Insert());

        return $this->row_added($ret);
    }
}

?>