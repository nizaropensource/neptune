/*
 *  neptuneJS
 */

var neptune = (function () {
    //Constructor
    (function () {
        if (window.addEventListener) { // W3C standard
            window.addEventListener('load', start, false);
            window.addEventListener('hashChange', hashChange, false);
        }
        else if (window.attachEvent) { // Microsoft
            window.attachEvent('onload', start);
            window.attachEvent('hashChange', hashChange);
        }
    })();

    /* Dictionary holding the Models, object structure
     *  key: model name
     *  .data: array holding model data
     */
    var models = {};

    /* Dictionary holding the Views, object structure
     *  key: view name(file name)
     *  .content: view content
     *  .attached_models: array holding models names in this view
     *
     */
    var views = {};

    /* id of div holding main page
     * default 'main-content'
     */
    var page_holder_id = "main-content";

    // Current visible page
    var current_page;

    // Enable or disable debug mode
    var debug = true;

    /* Log object to console, in debug mode only
     *  @obj(object): object to be written
     */
    function log(obj) {
        if (debug) {
            console.log(obj);
        }
    }

//Public

    /* Navigate @current_page to new @page
     *  @page(string): file name of the page in dir views/, can include subdirectories
     */
    function changePage(page) {
        current_page = page;
        window.location = "#" + page;
        loadView(page, page_holder_id);
    }

    // Reload current page
    function reloadPage() {
        changePage(current_page);
    }

    /* Loads @view_name into @element, and add it to @views
     *  @element(string/object): id of the element, or dom object
     */
    function loadView(view_name, element) {
        if (typeof element == 'string') {
            element = document.getElementById(element);
        }
        var obj = {
            element: element,
            view_name: view_name
        };
        // Tries to find it first in @views
        if (views[view_name]) {
            log('from neptune');
            loadViewCallback(views[view_name].content, obj);
        }
        else {
            ajax("views/" + view_name, loadViewCallback, "GET", obj);
        }
    }

    /* Gets model with name @model_name and adds it to @models, or fetch it from @models and dispatch model event
     *  @model_name(string): model name
     *  @callback(function): optional callback to be invoked with parameter model.data after loading the model
     */
    function getModel(model_name, callback) {
        // Tries to find it first in @models
        if (models[model_name] == null) {
            models[model_name] = {};
            ajax("models/index.php?model=" + model_name + "&operation=Select", getModelCallback, "GET", callback);
        }
        else if (models[model_name].data) {
            dispatchModelEvent(model_name);
            if (callback) {
                callback(models[model_name].data);
            }
        }
    }

    /* Submits the form in @elemet and update its local model
     *  @element(string/object): id of the element, or dom object
     *  @callback(fuction): optional callback to be invoked after submitting the form
     *  @callback_parameter(string/object): optional parameter to be sent with the callback
     */
    function saveModel(element, callback, callback_parameter) {
        if (typeof element == 'string') {
            element = document.getElementById(element);
        }
        var model = element.getAttribute("nt-data-model");
        var filter = element.getAttribute("nt-data-filter");
        var operation = element.getAttribute("nt-data-operation");
        var new_obj = prepareForm(element);
        if (!new_obj) {
            return;
        }

        var element_clone = element.cloneNode(true);
        element.parentNode.insertBefore(element_clone, element);

        var ifrm = document.createElement("IFRAME");
        document.getElementsByTagName("body")[0].appendChild(ifrm);

        if (!debug) {
            ifrm.setAttribute("style", "display: none");
        }
        else {
            ifrm.setAttribute("style", "float: left");
        }

        var idoc = ifrm.contentWindow.document;
        var form = idoc.createElement("FORM");
        
        if (operation) 
            form.setAttribute("action", "models/index.php?model="+model+"&operation="+operation);
        else
            form.setAttribute("action", "models/index.php?model="+model+"&operation=Update");
        form.setAttribute("method", "post");
        form.setAttribute("enctype", "multipart/form-data");
        //form.setAttribute("encoding", "multipart/form-data");
        form.appendChild(element);
        idoc.body.appendChild(form);
        form.submit();

        ifrm.onload = function () {
            element_clone.parentNode.insertBefore(element, element_clone);
            element_clone.parentNode.removeChild(element_clone);

            var response = ifrm.contentWindow.document.body.innerHTML;
            var res_obj = {};
            try {
                res_obj = JSON.parse(response);
            }
            catch (exception) {
                console.log(exception);
                return;
            }
            var custom_response = res_obj.data;
            delete res_obj.data;
            addObjectToModel(new_obj, res_obj, model, filter);

            if (callback) {
                callback(custom_response, callback_parameter);
            }
            if (!debug) {
                setTimeout(function () {
                    ifrm.parentNode.removeChild(ifrm);
                }, 1000);
            }
        };
    }

    /* AJAX request
     *  @path(string): path of file on server
     *  @callback(function): function to call after completion of the request
     *  @req_type(string): 'GET' or 'POST' request, default 'GET'
     *  @parameters(string/object): parameters to send with callback
     *  @post_data(string): data to be sent in the request
     */
    var ajax = function (path, callback, req_type, parameters, post_data) {
        req_type = req_type || "GET";
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //document.getElementById("loading_state").innerHTML = "Done." ;
                callback(xmlhttp.responseText, parameters);
            }
        }
        xmlhttp.open(req_type, path, true);
        if (req_type == "GET") {
            xmlhttp.send();
        }
        else {
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(post_data);
        }

        //document.getElementById("loading_state").innerHTML = "Loading ..." ;
    }

//Private

    // Executed after window loaded, to start neptune
    function start() {
        hashChange();
    }

    // Main usage to handle history back button
    function hashChange() {
        var p = window.location.hash.substring(1, window.location.hash.length);
        if (p == current_page || p == "") {
            return;
        }
        changePage(p);
    }

    /* callback of loadView, adds the requested view to @views, and start the process of filling @obj.element
     *  @view_content(string): content of the requested view
     *  @obj(object): {view_name, element} 
     */
    function loadViewCallback(view_content, obj) {
        views[obj.view_name] = {
            name: obj.view_name,
            content: view_content,
            attached_models: []
        }
        addContentToView(view_content, obj);
        extractModelsFrom(obj, true);
    }

    /* Adds @content to @obj.element, and execute scripts tags as javascript
     *  @content(string): content to fill with
     *  @obj(object): {view_name, element}
     */
    function addContentToView(content, obj) {
        obj.element.innerHTML = content;
        var hold = obj.element.getElementsByTagName("script");
        for (var i = 0; i < hold.length; i++) {
            if (hold[i].src) {
                document.getElementsByTagName("head")[0].appendChild(hold[i]);
            }
            else {
                eval(hold[i].innerHTML);
            }
        }
    }

    /* Extracts models names from @obj.element children and loads them and recursively over @obj.element's children
     *  @obj(object): {view_name, element}
     *  @root(boolean): true means non-recursive call, false means recursive call
     */
    function extractModelsFrom(obj, root) {
        var view_models = [];

        var hold = obj.element.children;
        // Extract models names from @obj.elemet children
        for (var i = 0; i < hold.length; i++) {
            if (hold[i].getAttribute('nt-data-model')) {
                var model_name = hold[i].getAttribute('nt-data-model');
                var original_html = hold[i].innerHTML;
                var filter = hold[i].getAttribute('nt-data-filter');
                var alias = hold[i].getAttribute('nt-alias');

                // Remove %model.key% from the view
                if (filter == 'new') {
                    var skip_obj = obj;
                    skip_obj.element = hold[i];
                    hold[i].innerHTML = renderHtmlWithModel(original_html, model_name, filter, alias);
                    extractModelsFrom(skip_obj, false);
                    continue;
                }

                (function (_element, _original_html) {
                    // Attatch model to  @_element
                    _element.addEventListener('model-' + _element.getAttribute("nt-data-model"), function (event) {
                        var filter = _element.getAttribute('nt-data-filter');
                        var alias = _element.getAttribute('nt-alias');
                        // Render @obj.element with model data using %model.key% syntax
                        _element.innerHTML = renderHtmlWithModel(_original_html, event.detail, filter, alias);
                        var _obj = {
                            element: _element,
                            view_name: obj.view_name
                        };
                        _obj.element = _element;
                        // Call again with children of @obj.element
                        extractModelsFrom(_obj, false);
                    });
                })(hold[i], original_html);

                view_models.push(hold[i].getAttribute("nt-data-model"));
            }
            else {
                var leaf_obj = {
                    element: hold[i],
                    view_name: obj.view_name
                };
                extractModelsFrom(leaf_obj, false);
            }
        }

        // Add extracted models names to view
        views[obj.view_name].attached_models = view_models;

        //Load view models
        for (var i = 0; i < view_models.length; i++) {
            getModel(view_models[i]);
        }
    }

    /* callback of getModel
     *  @model_conent(string): JSON string of the model data
     *  @callback(function): optional callback
     */
    function getModelCallback(model_content, callback) {
        // Parse json model_content to js object
        var model_obj;
        try {
            model_obj = JSON.parse(model_content);
        }
        catch (exception) {
            console.log(exception);
            return;
        }

        models[model_obj.model].data = model_obj.data;

        dispatchModelEvent(model_obj.model);
        if (callback) {
            callback(model_obj.data);
        }
    }

    /* Dispatch model event to update the views using it
     *  @model_name(string): model name to dispatch its event named 'model-@model_name'
     */
    function dispatchModelEvent(model_name) {
        var event = new CustomEvent('model-' + model_name, {
            detail: model_name,
            bubbles: false,
            cancelable: true
        });
        spreadEvent(event);
    }

    /* Spread event over all DOM elements
     * Better to spread over elements listening on this event only
     *  @event(CustomEvent): event of the model
     */
    function spreadEvent(event) {
        var allElements = document.getElementsByTagName('*');
        for (var i = 0; i < allElements.length; i++)
            allElements[i].dispatchEvent(event);
    }

    /* Replace %model.key% or %key% with real value from the model data, and repeat @html with count of rows in the model
     *  @html(string): original html
     *  @model_name(string): model name
     *  @filter(string/array): new or key=value,key=value,..., filter to applied on model data appearing in @html
     *  @alias(string): just an alias for the model used in redering only, %alias.key%
     */
    function renderHtmlWithModel(html, model_name, filter, alias) {
        // console.log(model_name);
        console.log(models);
        html = convertCharEntities(html);
        var model = models[model_name].data;
        var res = '';

        if (filter && filter == 'new') {
            res = html;
            if (model.length) {
                for (var key in model[0]) {
                    var regex = new RegExp("%" + key + "%", 'g');
                    var regex2 = new RegExp("%" + model_name + "." + key + "%", 'g');
                    var regex3 = new RegExp("%" + alias + "." + key + "%", 'g');
                    res = res.replace(regex, '');
                    res = res.replace(regex2, '');
                    res = res.replace(regex3, '');
                }
            }
        }
        else {
            filter = filter ? filter.split(',') : [];
            var match = true;
            var row = '';
            for (var i = 0; i < model.length; i++) {
                row = html;

                match = true;
                for (var j = 0; j < filter.length; j++) {
                    // TODO apply >=, <= , >, =, <
                    var filter_item = filter[j].split('=');
                    if (model[i][filter_item[0]] && model[i][filter_item[0]] != filter_item[1]) {
                        match = false;
                        break;
                    }
                }

                if (match) {
                    for (var key in model[i]) {
                        var regex = new RegExp("%" + key + "%", 'g');
                        var regex2 = new RegExp("%" + model_name + "." + key + "%", 'g');
                        var regex3 = new RegExp("%" + alias + "." + key + "%", 'g');
                        row = row.replace(regex, model[i][key]);
                        row = row.replace(regex2, model[i][key]);
                        row = row.replace(regex3, model[i][key]);
                    }
                    res += row;
                }
            }
        }

        return res;
    }

    /* Validates the form in @elemnt over attribute nt-data-regex, and collects form data
     *  @element(object): dom object containing the form to be validated
     */
    function prepareForm(element) {
        var hold = element.querySelectorAll("input,select,textarea");
        var obj = {};
        for (var i = 0; i < hold.length; i++) {
            //var val = hold[i].tagName == "TEXTAREA" ? hold[i].innerHTML : hold[i].value;
            var val= hold[i].value ;
            if (hold[i].getAttribute("nt-data-regex")) {
                if (!val.match(new RegExp(hold[i].getAttribute("nt-data-regex")))) {
                    hold[i].focus();
                    return false;
                }
            }
            if (hold[i].tagName == 'SELECT' && hold[i].hasAttribute('nt-property-name')) {
                var select = hold[i];
                obj[select.getAttribute('nt-property-name')] = select[select.selectedIndex].innerHTML;
            }
            obj[hold[i].getAttribute('name')] = val;
        }
        return obj;
    }

    /* Merge @obj with @res_obj and this merged object to @models[@model], then dispatch model event
     *  @obj(object): local object
     *  @res_obj(object): response object
     *  @model_name(string): model name
     *  @filter(string/array): new or key=value,key=value,...,
     */
    function addObjectToModel(obj, res_obj, model_name, filter) {
        if (!models[model_name]) {
            return;
        }

        var model = models[model_name].data;

        for (key in res_obj)
            obj[key] = res_obj[key];

        if (filter == "new") {
            models[model_name].data.push(obj);
        }
        else {
            var pk_name = obj['pk'];

            for (var i = 0; i < model.length; i++) {
                if (obj[pk_name] && model[i][pk_name] && model[i][pk_name] == obj[pk_name]) {
                    for (var key in model[i]) {
                        model[i][key] = obj[key];
                    }
                }
            }
        }

        dispatchModelEvent(model_name);
    }

    /* Convert char entities
     * @str(string): string to be converted
     */
    function convertCharEntities(str) {
        return str.replace('&amp;', '&')
        .replace('&gt;', '>')
        .replace('&lt;', '<')
        .replace('&quot;', '"')
        .replace('&#39;', "'");
    }

    // Specify public functions
    return {
        changePage: changePage,
        reloadPage: reloadPage,
        loadView: loadView,
        getModel: getModel,
        saveModel: saveModel,
        ajax: ajax,
        page_id: page_holder_id
    }

})();