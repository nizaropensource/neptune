 <?php
?>
<!DOCTYPE html>

<html>
    
    <head>
        <title>Restaurants</title>
        <link rel="stylesheet" href="css/bootstrap.css"/>
        <link rel="stylesheet" href="css/main.css"/>
        
        <script type="text/javascript" src="controllers/neptuneJS2.js"></script>
        <script type="text/javascript" src="controllers/js.js"></script>
      
    </head>
    
    <body>
        <div id="main">
            <div class="row">
                <div class="col-md-3">
                    <button class="btn" onclick="javascript:neptune.changePage('restaurants.php')">Restaurants</button><br/><br/>
                    <button class="btn" onclick="javascript:neptune.changePage('foods.php')">Foods</button><br/><br/>
                    <button class="btn" onclick="javascript:neptune.changePage('ingredients.php')">Ingredients</button><br/><br/>
                    <button class="btn" onclick="javascript:neptune.changePage('menu.php')">Menu</button><br/><br/>
                </div>
                <div class="col-md-9">
                    <div id="main-content"></div>
                </div>
            </div>
        </div>
    </body>
    
</html>